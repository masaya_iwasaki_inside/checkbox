# coding=utf-8
from __future__ import division
import os
import cv2
import numpy as np
import random
import math
import scipy.stats as stats
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter

BACKGROUND_PATH = os.getenv('BACKGROUND_PATH', 'background')


def zoom_out(image, sigma=0.1):
    ratio = truncated_norm(1, 1.5, 1, sigma)
    out_image = np.ones((int(image.shape[0] * ratio), int(image.shape[1] * ratio), 3), dtype=np.uint8) * 255
    top = (int((out_image.shape[0] - image.shape[0]) / 2), int((out_image.shape[1] - image.shape[1]) / 2))
    out_image[top[0]:top[0] + image.shape[0], top[1]:top[1] + image.shape[1], :] = image
    return out_image


def truncated_norm(lower, upper, mu, sigma, to_int=False):
    value = stats.truncnorm((lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma).rvs()
    return int(round(value)) if to_int else value


def saltpepper(image, salt=0.001, pepper=0.001):
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            r = np.random.rand()
            if r < salt:
                image[x, y, :] = (255, 255, 255)
            elif r > 1 - pepper:
                image[x, y, :] = (0, 0, 0)
    return image


def random_gaussianBlur(image, ave=2, sig=2):
    kernel_size = random.choice([3, 5, 7, 9])
    sigma = truncated_norm(0, ave+(2*sig), ave, sig)
    image = cv2.GaussianBlur(image, (kernel_size, kernel_size), sigma, sigma)
    return image


def random_blur(image):
    kernels = [np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]]),
               np.array([[0.0625, 0.0625, 0.0625], [0.0625, 0.5, 0.0625], [0.0625, 0.0625, 0.0625]]),
               np.array([[0.111, 0.111, 0.111], [0.111, 0.111, 0.111], [0.111, 0.111, 0.111]])]

    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    filtered_img = image.copy()

    for y in range(1, image.shape[0]-1):
        for x in range(1, image.shape[1]-1):
            # kernel = random.choice(kernels)
            kernel = random.choice(kernels)
            # kernel = kernels[3]
            value = 0
            for i in range(-1, 2):
                for j in range(-1, 2):
                    value += image[y+i, x+j] * kernel[i+1, j+1]
            filtered_img[y, x] = int(value)

    return cv2.cvtColor(filtered_img, cv2.COLOR_GRAY2BGR)


def random_transform_aspect(image):
    h, w, _ = image.shape
    output_img = np.zeros(image.shape, dtype=np.uint8) + 255
    fx = truncated_norm(0.7, 1, 1, 0.02)
    fy = truncated_norm(0.7, 1, 1, 0.02)
    size = (int(max(1, fx * image.shape[1])), int(max(1, fy * image.shape[0])))
    image = cv2.resize(image, size)
    output_img[:image.shape[0], :image.shape[1]] = image
    return output_img


def gamma_correction(image, gamma):
    image = 255.0 * (image / 255.0) ** (1/gamma)
    return image


def image_compression(image):
    ratio = truncated_norm(0.5, 1, 1, 0.25)
    image = cv2.resize(image, None, fx=ratio, fy=ratio)
    image = cv2.resize(image, (image.shape[1], image.shape[0]))
    return image

def random_rotate(image, c_sigma=10, rad_sigma=2):
    image = 255 - image
    size = tuple(np.array([image.shape[1], image.shape[0]]))
    rad = truncated_norm(-(rad_sigma*2), (rad_sigma*2), 0, rad_sigma)
    center = [image.shape[1] / 2, image.shape[0] / 2]
    center = tuple(np.array([truncated_norm(center[0]-(2*c_sigma), center[0]+(2*c_sigma), center[0], c_sigma),
                             truncated_norm(center[1]-(2*c_sigma), center[1]+(2*c_sigma), center[1], c_sigma)]))
    rotation_matrix = cv2.getRotationMatrix2D(center, rad, 1)

    return 255 - cv2.warpAffine(image, rotation_matrix, size, flags=cv2.INTER_CUBIC)


def random_line(img, line_num=2, thick_ave=1, style='line'):
    points = [[(random.randint(0, img.shape[1]), random.randint(0, img.shape[0])),
               (random.randint(0, img.shape[1]), random.randint(0, img.shape[0]))] for _ in range(line_num)]
    thick = truncated_norm(1, thick_ave*3, thick_ave, thick_ave, to_int=True)
    color = random.choice([(0, 0, 0), (255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (0, 255, 255),
                           (255, 255, 255), (128, 128, 128)])

    if style == 'line':
        for ps in points:
            cv2.line(img, ps[0], ps[1], color, thick)
    if style == 'dot':
        for ps in points:
            drawdotline(img, ps[0], ps[1], color, thick)
    return img


def drawdotline(img, pt1, pt2, color=(0, 0, 0), thickness=1):
    gap = truncated_norm(2, 50, 15, 3, to_int=True)
    dist = ((pt1[0] - pt2[0]) ** 2 + (pt1[1] - pt2[1]) ** 2) ** .5
    pts = []
    for i in np.arange(0, dist, gap):
        r = i / dist
        x = int((pt1[0] * (1 - r) + pt2[0] * r) + .5)
        y = int((pt1[1] * (1 - r) + pt2[1] * r) + .5)
        p = (x, y)
        pts.append(p)
    for p in pts:
        cv2.circle(img, p, thickness, color, -1)


def drawpoly(img, pts, color, thickness=1):
    s = pts[0]
    e = pts[0]
    pts.append(pts.pop(0))
    for p in pts:
        s = e
        e = p
        drawdotline(img, s, e, color, thickness)
    return img


def drawcircle(img, center, xr, yr, color=(150, 150, 150), thickness=1):
    theta = 0
    inter_theta = truncated_norm(5, 20, 10, 3, to_int=True)

    while theta <= 360:
        x = xr * math.cos(math.pi * (theta / 180))
        y = yr * math.sin(math.pi * (theta / 180))
        if (theta > 45 and theta < 135) or (theta > 225 and theta < 315):
            cv2.rectangle(img, (center[0] + int(x), center[1] + int(y)),
                          (center[0] + int(x) + 1, center[1] + int(y)),
                          color, -1)
        else:
            cv2.rectangle(img, (center[0] + int(x), center[1] + int(y)),
                          (center[0] + int(x), center[1] + int(y) + 1),
                          color, -1)

        theta += inter_theta
    return img


def emphasize_white(img, mask=True, org=False):
    img_org = img.copy()
    min = np.min(img)
    max = np.max(img)

    if max-min == 0:
        return img
    img = (img - min) * (255 / (max-min))
    img = img.astype(dtype=np.uint8)

    if mask:
        grayscaled = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, otsu = cv2.threshold(grayscaled, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        if org:
            img = cv2.bitwise_and(img_org, img_org, mask=otsu)
        else:
            img = cv2.bitwise_and(img, img, mask=otsu)
    else:
        img = cv2.cvtColor(otsu, cv2.COLOR_GRAY2BGR)
    return img

def elastic_transform(image, alpha, sigma, random_state=None):
    """Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
    """
    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = image.shape
    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant",
                         cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant",
                         cval=0) * alpha

    x, y = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing='ij')
    indices = np.reshape(x + dx, (-1, 1)), np.reshape(y + dy, (-1, 1))
    return map_coordinates(image, indices, order=1).reshape(shape)

def rotate_and_shift(src_image, rad, x, y):
    src_image = 255 - src_image
    size = tuple(np.array([src_image.shape[1], src_image.shape[0]]))
    rad = rad * np.pi / 180.0
    matrix = [
        [np.cos(rad), -1 * np.sin(rad), x],
        [np.sin(rad), np.cos(rad), y]
    ]
    affine_matrix = np.float32(matrix)
    return 255 - cv2.warpAffine(src_image, affine_matrix, size, flags=cv2.INTER_LINEAR)

def extract_line(img, margin_h=5, margin_w=3, thread=0.01):
    def remove_region(char_region, thre=2):
        norm_char_region = []
        for region in char_region:
            if not (region[1] - region[0]) < thre:
                norm_char_region.append(region)
        return norm_char_region

    def cut_image(img, margin, thread, is_wide=True):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # gray = cv2.threshold(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        # gray = cv2.threshold(gray, 120, 255, cv2.THRESH_BINARY)[1]
        gray = 255 - gray
        if is_wide:
            gray = gray.transpose()

        hist = [sum(temp) for temp in gray]
        hist_max, hist_min = max(hist), min(hist)
        hist = [(h - hist_min) / hist_max for h in hist]

        char_region = []
        start, is_back = 0, True
        for (i, h) in enumerate(hist):
            if is_back and h > thread:
                start = i
                is_back = False
            if not is_back and h < thread:
                char_region.append((start, i))
                is_back = True
        if len(char_region) == 0:
            char_region.append((start, len(hist) - 1))
        elif not char_region[-1][0] == start:
            char_region.append((start, len(hist) - 1))
        char_region = remove_region(char_region, 3)

        if len(char_region) == 0:
            return img

        if not is_wide:
            cut_point = char_region[0]
            max_area = (cut_point[1] - cut_point[0])
            for region in char_region[1:]:
                area = (region[1] - region[0])
                if area > max_area:
                    max_area = area
                    cut_point = region
            cut_point = (max(cut_point[0] - margin, 0), min(cut_point[1] + margin, len(hist)))
            img = img[cut_point[0]:cut_point[1], :]
        else:
            cut_point = (max(char_region[0][0]-margin, 0), min(char_region[-1][1]+margin, len(hist)))
            img = img[:, cut_point[0]:cut_point[1]]
        return img

    img = cut_image(img, margin_h, thread, is_wide=False)
    img = cut_image(img, margin_w, thread, is_wide=True)
    return img
