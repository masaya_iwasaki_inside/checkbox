## checkbox学習用スクリプト

#### 学習・検証データの用意
```Bash
# 学習データ
mkdir ../dataset
cd ../dataset
aws s3 cp s3://ml-ops-learning-data.inside.ai/dataset/checkbox/checkbox.zip .
unzip checkbox.zip
```
#### 実行方法
```Bash
python3 train.py
```

### スポットでの環境構築
```Bash
sudo pip3 install opencv-python
sudo pip3 install python-Levenshtein
sudo pip3 install redis
sudo pip3 install Keras -U
sudo pip3 install tensorflow-gpu -U
```