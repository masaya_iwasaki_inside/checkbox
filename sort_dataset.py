#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""feature detection."""

import cv2
import os


def compere_shift_img(comparing_img, bf, detector, target_des, img_size):
    try:
        comparing_img = cv2.resize(comparing_img, img_size)
        (comparing_kp, comparing_des) = detector.detectAndCompute(comparing_img, None)
        matches = bf.match(target_des, comparing_des)
        dist = [m.distance for m in matches]
        ret = sum(dist) / len(dist)
    except cv2.error:
        ret = 100000

    return ret


def compere_hist_img(comparing_img, target_hist):
    comparing_hist = cv2.calcHist([comparing_img], [0], None, [256], [0, 256])
    comparing_hist[220:] = 0.
    ret = cv2.compareHist(target_hist, comparing_hist, 0)
    print(comparing_hist)
    return ret


img_size = (100, 100)

target_img_path = 'img1.png'
comparing_img_path = 'img3.png'


bf = cv2.BFMatcher(cv2.NORM_HAMMING)
# detector = cv2.ORB_create()
detector = cv2.AKAZE_create()

target_img = cv2.imread(target_img_path, cv2.IMREAD_GRAYSCALE)
target_hist = cv2.calcHist([target_img], [0], None, [256], [0, 256])
(target_kp, target_des) = detector.detectAndCompute(cv2.resize(target_img, img_size), None)
target_hist[220:] = 0.
print(target_hist)

comparing_img = cv2.imread(comparing_img_path, cv2.IMREAD_GRAYSCALE)

ret = compere_hist_img(comparing_img, target_hist)
# ret = compere_img(comparing_img, bf, detector, target_des, img_size)
print(ret)
