# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
sys.path.append('../')

import argparse

from model import CRNN
from data_gene import Data_gen

parser = argparse.ArgumentParser(description='train the model for all model')
parser.add_argument('--gpu_count', type=int, default=1)
parser.add_argument('--load_model_path', type=str, default='')
parser.add_argument('--generate_mode', type=str, default='tegaki')
parser.add_argument('--dataset_path', type=str, default='../dataset/')
parser.add_argument('--cnn_model', type=str, default='mobilenet')
parser.add_argument('--marksheet', type=bool, default=False)
args = parser.parse_args()

gpu_count = args.gpu_count
batch_size = 16

epochs = 1000
steps_per_epoch = 10000
validation_steps = 1
max_length = 20
min_length = 1
input_shape = (64, 256, 3)
generate_mode = args.generate_mode
dataset_path = args.dataset_path
is_marksheet = args.marksheet
valid_path = dataset_path+'checkbox/checkbox_valid_all/'
valid_list = [ # {'path': dataset_path + 'checkbox/checkbox_valid1', 'dataset': 'valid1', 'reverse': False},
              {'path': dataset_path + 'checkbox/checkbox_valid_all', 'dataset': 'valid', 'reverse': False},
              # {'path': dataset_path + 'checkbox/checkbox_valid_old', 'dataset': 'valid_old', 'reverse': False},
              ]

multithread = True
cnn_model = args.cnn_model  # 'xception' 'densenet121' 'mobilenet' 'VGG16' 'similar_googlenet' 'xception_slim'
prefix_model_name = ('checkbox_' + cnn_model)
load_model_path = args.load_model_path

data_gen = Data_gen(input_shape=input_shape, max_length=max_length, min_length=min_length, batch_size=batch_size,
                    dataset_path=dataset_path, is_marksheet=is_marksheet)
valid_data = data_gen.gen_valid(valid_path=valid_path)

model = CRNN(prefix_model_name=prefix_model_name, input_shape=input_shape, classes=data_gen.get_classes(),
             output_model_path='./', gpu_count=gpu_count, load_model_weights_path=load_model_path, cnn_model=cnn_model)

# plot model
model.save_plot_model('./')

model.fit_generator(train_generator=data_gen, validate_generator=valid_data,
                    validation_steps=validation_steps, steps_per_epoch=steps_per_epoch, epochs=epochs,
                    valid_list=valid_list, batch_size=batch_size, multithread=multithread)
