import os
import numpy as np
import pytz
from datetime import datetime
import h5py
import cv2

from utils import norm_batch, img_glob
from keras.layers import Input, Dense, GaussianNoise, Conv2D, BatchNormalization, Activation, SeparableConv2D,\
    MaxPooling2D, add, GlobalAveragePooling2D, GlobalMaxPooling2D, PReLU, Dropout, concatenate
from keras.models import Model
from keras.callbacks import Callback, LearningRateScheduler, CSVLogger
from keras.optimizers import SGD
from keras.utils import plot_model
from keras.applications.xception import Xception
from keras.applications.mobilenet import MobileNet
from keras.applications.densenet import DenseNet121
from keras.applications.vgg16 import VGG16

class TrainingCallback(Callback):
    def __init__(self, predict_model, prefix, output_dir_path, input_shape, valid_list, batch_size, classes):
        self.predict_model = predict_model
        self.output_dir_path = output_dir_path
        self.prefix = prefix
        self.input_shape = input_shape
        self.valid_list = valid_list
        self.batch_size = batch_size
        self.classes = classes + ['']

    def predict(self, model, batch_size, img_path, answer, reverse=False):
        img_path_list = img_glob(img_path)
        perfect_scores, results = [], {True: [], False: []}
        count = 1
        for i in range(0, len(img_path_list), batch_size):
            imgs = [cv2.imread(image_path) for image_path in img_path_list[i:i + batch_size]]
            imgs = norm_batch(imgs, constant_input_shape=self.input_shape)
            if reverse:
                imgs = 1 - imgs
            outs = model.predict(imgs)

            texts, likelihoods = [], []
            for o in outs:
                max_index = o.argmax()
                texts.append(self.classes[max_index])
                likelihoods.append(o[max_index])

            for (image_path, text, likelihood) in zip(img_path_list[i:i + batch_size], texts, likelihoods):
                prefix = '○' if answer == text else '×'
                print(count, '/', len(img_path_list), prefix, answer, text, likelihood, image_path.split('/')[-1])
                perfect_scores.append(1 if answer == text else 0)
                results[answer == text].append([text, answer, image_path, likelihood])
                count += 1
        return perfect_scores, results

    def output_log(self, log_file_name, results):
        f = open(log_file_name, 'w')
        for (pred, ans, path, likelihood) in results[False]:
            f.write('×\t' + ans + '\t' + pred + '\t' + path + '\t' + str(likelihood) + '\n')
        for (pred, ans, path, likelihood) in results[True]:
            f.write('◯\t' + ans + '\t' + pred + '\t' + path + '\t' + str(likelihood) + '\n')
        f.close()

    def on_epoch_end(self, epoch, logs=None):
        model = self.predict_model
        perfect_scores, results = {}, {True: [], False: []}
        for valid in self.valid_list:
            print(valid['dataset'])
            perfect_scores.update({valid['dataset']: []})
            for i in [0, 1]:
                valid_path = os.path.join(valid['path'], str(i))
                perfect_scores_, results_ = self.predict(model, self.batch_size, valid_path, self.classes[i], reverse=valid['reverse'])
                perfect_scores[valid['dataset']] += perfect_scores_
                results[True] += results_[True]
                results[False] += results_[False]


        time_stamp = datetime.strftime(datetime.now(pytz.timezone('Japan')), '%m%d%H%M')

        perfects_str = ''
        for dataset in perfect_scores:
            perfect_score = 1.0 * sum(perfect_scores[dataset]) / len(perfect_scores[dataset])
            perfects_str = '{0}_{1}_acc_{2:.5f}'.format(perfects_str, dataset, perfect_score)

        model_file_name = '{0}_{1}_epoch_{2:03d}_loss_{3:.5f}{4}.hdf5'.format(
            time_stamp, self.prefix, epoch + 1, logs['loss'], perfects_str)
        model_file_name = os.path.join(self.output_dir_path, model_file_name)

        log_file_name = model_file_name.replace('.hdf5', '.txt')
        self.output_log(log_file_name, results)

        model.save(model_file_name)

        with h5py.File(model_file_name, 'a') as h5file:
            dir = 'ai_inside'
            h5file.create_group(dir)
            h5file.create_dataset(os.path.join(dir, 'classes'),
                                  (len(self.classes), 1), data=[_.encode('utf8') for _ in self.classes])
        print('\n----- The model weight was saved to ' + model_file_name + ' -----\n')


class CRNN:
    def __init__(self, prefix_model_name, input_shape, classes, output_model_path,
                 gpu_count, load_model_weights_path='', cnn_model='xception'):
        self.prefix_model_name = prefix_model_name
        self.input_shape = input_shape
        self.output_model_path = output_model_path
        self.gpu_count = gpu_count
        self.load_weight_flag = False
        self.classes = classes
        self.num_classes = len(self.classes)+1
        self.cnn_trainable = True
        self.out_trainable = True
        self.cnn_model = cnn_model
        self.prepare_model()
        self._load_model(load_model_weights_path)

        os.makedirs(output_model_path, exist_ok=True)

    def _load_model(self, load_weight_path):
        if len(load_weight_path) > 0:
            self.train_model.load_weights(load_weight_path, by_name=True)
            self.load_weight_flag = True
            print('load model' + load_weight_path)

    def prepare_model(self):
        input_tensor = Input(shape=self.input_shape)
        cnn_dict = {'de'
                    '': Xception, 'mobilenet': MobileNet, 'densenet121': DenseNet121, 'VGG16': VGG16,
                    'xception_slim': self.xception_slim, 'similar_googlenet': self.similar_googlenet,
                    }

        cnn = cnn_dict[self.cnn_model](input_tensor=input_tensor, weights='imagenet', include_top=False, pooling='avg')
        cnn = Dense(512, activation="relu")(cnn.output)
        output_softmax = Dense(len(self.classes), activation="softmax")(cnn)
        model = Model(input_tensor, output_softmax)
        self.train_model = model

    def xception_slim(self, include_top=True, weights='', input_tensor=None, input_shape=None,
                                pooling=None, classes=1000):
        if input_tensor is None:
            img_input = Input(shape=input_shape)
        else:
            img_input = input_tensor

        x = Conv2D(32, (3, 3), strides=(2, 2), use_bias=False, name='block1_conv1')(img_input)
        x = BatchNormalization(name='block1_conv1_bn')(x)
        x = Activation('relu', name='block1_conv1_act')(x)
        x = Conv2D(64, (3, 3), use_bias=False, name='block1_conv2')(x)
        x = BatchNormalization(name='block1_conv2_bn')(x)
        x = Activation('relu', name='block1_conv2_act')(x)

        residual = Conv2D(128, (1, 1), strides=(2, 2),
                          padding='same', use_bias=False)(x)
        residual = BatchNormalization()(residual)

        x = SeparableConv2D(128, (3, 3), padding='same', use_bias=False, name='block2_sepconv1')(x)
        x = BatchNormalization(name='block2_sepconv1_bn')(x)
        x = Activation('relu', name='block2_sepconv2_act')(x)
        x = SeparableConv2D(128, (3, 3), padding='same', use_bias=False, name='block2_sepconv2')(x)
        x = BatchNormalization(name='block2_sepconv2_bn')(x)

        x = MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block2_pool')(x)
        x = add([x, residual])

        residual = Conv2D(256, (1, 1), strides=(2, 2),
                          padding='same', use_bias=False)(x)
        residual = BatchNormalization()(residual)

        x = Activation('relu', name='block3_sepconv1_act')(x)
        x = SeparableConv2D(256, (3, 3), padding='same', use_bias=False, name='block3_sepconv1')(x)
        x = BatchNormalization(name='block3_sepconv1_bn')(x)
        x = Activation('relu', name='block3_sepconv2_act')(x)
        x = SeparableConv2D(256, (3, 3), padding='same', use_bias=False, name='block3_sepconv2')(x)
        x = BatchNormalization(name='block3_sepconv2_bn')(x)

        x = MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block3_pool')(x)
        x = add([x, residual])

        residual = Conv2D(728, (1, 1), strides=(2, 2),
                          padding='same', use_bias=False)(x)
        residual = BatchNormalization()(residual)

        x = Activation('relu', name='block4_sepconv1_act')(x)
        x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name='block4_sepconv1')(x)
        x = BatchNormalization(name='block4_sepconv1_bn')(x)
        x = Activation('relu', name='block4_sepconv2_act')(x)
        x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name='block4_sepconv2')(x)
        x = BatchNormalization(name='block4_sepconv2_bn')(x)

        x = MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block4_pool')(x)
        x = add([x, residual])

        for i in range(2):
            residual = x
            prefix = 'block' + str(i + 5)

            x = Activation('relu', name=prefix + '_sepconv1_act')(x)
            x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name=prefix + '_sepconv1')(x)
            x = BatchNormalization(name=prefix + '_sepconv1_bn')(x)
            x = Activation('relu', name=prefix + '_sepconv2_act')(x)
            x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name=prefix + '_sepconv2')(x)
            x = BatchNormalization(name=prefix + '_sepconv2_bn')(x)
            x = Activation('relu', name=prefix + '_sepconv3_act')(x)
            x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name=prefix + '_sepconv3')(x)
            x = BatchNormalization(name=prefix + '_sepconv3_bn')(x)

            x = add([x, residual])

        residual = Conv2D(1024, (1, 1), strides=(2, 2),
                          padding='same', use_bias=False)(x)
        residual = BatchNormalization()(residual)

        x = Activation('relu', name='block13_sepconv1_act')(x)
        x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False, name='block13_sepconv1')(x)
        x = BatchNormalization(name='block13_sepconv1_bn')(x)
        x = Activation('relu', name='block13_sepconv2_act')(x)
        x = SeparableConv2D(1024, (3, 3), padding='same', use_bias=False, name='block13_sepconv2')(x)
        x = BatchNormalization(name='block13_sepconv2_bn')(x)

        x = MaxPooling2D((3, 3), strides=(2, 2), padding='same', name='block13_pool')(x)
        x = add([x, residual])

        x = SeparableConv2D(1536, (3, 3), padding='same', use_bias=False, name='block14_sepconv1')(x)
        x = BatchNormalization(name='block14_sepconv1_bn')(x)
        x = Activation('relu', name='block14_sepconv1_act')(x)

        x = SeparableConv2D(2048, (3, 3), padding='same', use_bias=False, name='block14_sepconv2')(x)
        x = BatchNormalization(name='block14_sepconv2_bn')(x)
        x = Activation('relu', name='block14_sepconv2_act')(x)

        if include_top:
            x = GlobalAveragePooling2D(name='avg_pool')(x)
            x = Dense(classes, activation='softmax', name='predictions')(x)
        else:
            if pooling == 'avg':
                x = GlobalAveragePooling2D()(x)
            elif pooling == 'max':
                x = GlobalMaxPooling2D()(x)

        model = Model(img_input, x, name='xception_slim')

        return model

    def similar_googlenet(self, include_top=True, weights='', input_tensor=None, input_shape=None,
                                pooling=None, classes=1000):
        if input_tensor is None:
            img_input = Input(shape=input_shape)
        else:
            img_input = input_tensor
        max_pool_size = (2, 2)

        cnn = img_input

        #conv1
        #conv1_1
        cnn1_1 = Conv2D(32, (3, 3), padding='same', name='conv1_1')(cnn)
        cnn1_1 = BatchNormalization()(cnn1_1)
        cnn1_1 = PReLU()(cnn1_1)
        cnn1_1 = MaxPooling2D(pool_size=max_pool_size)(cnn1_1)
        cnn1_1 = Dropout(0.1)(cnn1_1)

        #conv1_2
        cnn1_2 = Conv2D(32, (5, 5), padding='same', name='cnn1_2')(cnn)
        cnn1_2 = BatchNormalization()(cnn1_2)
        cnn1_2 = PReLU()(cnn1_2)
        cnn1_2 = MaxPooling2D(pool_size=max_pool_size)(cnn1_2)
        cnn1_2 = Dropout(0.1)(cnn1_2)

        #conv1_3
        cnn1_3 = Conv2D(32, (7, 7), padding='same', dilation_rate=(2, 2), name='cnn1_3')(cnn)
        cnn1_3 = BatchNormalization()(cnn1_3)
        cnn1_3 = PReLU()(cnn1_3)
        cnn1_3 = MaxPooling2D(pool_size=max_pool_size)(cnn1_3)
        cnn1_3 = Dropout(0.1)(cnn1_3)

        #conv1_4
        conv1_4 = Conv2D(32, (1, 1), padding='same', name='conv1_4')(cnn)
        conv1_4 = BatchNormalization()(conv1_4)
        conv1_4 = PReLU()(conv1_4)
        conv1_4 = MaxPooling2D(pool_size=max_pool_size)(conv1_4)
        conv1_4 = Dropout(0.1)(conv1_4)

        cnn1 = concatenate([cnn1_1, cnn1_2, cnn1_3, conv1_4])

        #conv2
        #conv2_1
        cnn2_1 = Conv2D(64, (3, 3), padding='same', name='conv2_1')(cnn1)
        cnn2_1 = BatchNormalization()(cnn2_1)
        cnn2_1 = PReLU()(cnn2_1)
        cnn2_1 = MaxPooling2D(pool_size=max_pool_size)(cnn2_1)
        cnn2_1 = Dropout(0.1)(cnn2_1)

        #conv2_2
        cnn2_2 = Conv2D(64, (5, 5), padding='same', name='cnn2_2')(cnn1)
        cnn2_2 = BatchNormalization()(cnn2_2)
        cnn2_2 = PReLU()(cnn2_2)
        cnn2_2 = MaxPooling2D(pool_size=max_pool_size)(cnn2_2)
        cnn2_2 = Dropout(0.1)(cnn2_2)

        #conv2_3
        conv2_3 = Conv2D(64, (7, 7), padding='same', dilation_rate=(2, 2), name='conv2_3')(cnn1)
        conv2_3 = BatchNormalization()(conv2_3)
        conv2_3 = PReLU()(conv2_3)
        conv2_3 = MaxPooling2D(pool_size=max_pool_size)(conv2_3)
        conv2_3 = Dropout(0.1)(conv2_3)

        #conv2_4
        conv2_4 = Conv2D(64, (1, 1), padding='same', name='conv2_4')(cnn1)
        conv2_4 = BatchNormalization()(conv2_4)
        conv2_4 = PReLU()(conv2_4)
        conv2_4 = MaxPooling2D(pool_size=max_pool_size)(conv2_4)
        conv2_4 = Dropout(0.1)(conv2_4)

        cnn2 = concatenate([cnn2_1, cnn2_2, conv2_3, conv2_4])

        #conv3
        #conv3_1
        cnn3_1 = Conv2D(96, (3, 3), padding='same', name='cnn3_1')(cnn2)
        cnn3_1 = BatchNormalization()(cnn3_1)
        cnn3_1 = PReLU()(cnn3_1)
        cnn3_1 = Dropout(0.1)(cnn3_1)

        #conv3_2
        cnn3_2 = Conv2D(96, (5, 5), padding='same', name='cnn3_2')(cnn2)
        cnn3_2 = BatchNormalization()(cnn3_2)
        cnn3_2 = PReLU()(cnn3_2)
        cnn3_2 = Dropout(0.1)(cnn3_2)

        #conv3_3
        conv3_3 = Conv2D(96, (7, 7), padding='same', dilation_rate=(2, 2), name='conv3_3')(cnn2)
        conv3_3 = BatchNormalization()(conv3_3)
        conv3_3 = PReLU()(conv3_3)
        conv3_3 = Dropout(0.1)(conv3_3)

        #conv3_4
        conv3_4 = Conv2D(96, (1, 1), padding='same', name='conv3_4')(cnn2)
        conv3_4 = BatchNormalization()(conv3_4)
        conv3_4 = PReLU()(conv3_4)
        conv3_4 = Dropout(0.1)(conv3_4)

        cnn3 = concatenate([cnn3_1, cnn3_2, conv3_3, conv3_4])

        #conv4
        #conv4_1
        conv4_1 = Conv2D(128, (3, 3), padding='same', name='conv4_1')(cnn3)
        conv4_1 = BatchNormalization()(conv4_1)
        conv4_1 = PReLU()(conv4_1)
        conv4_1 = MaxPooling2D(pool_size=max_pool_size)(conv4_1)
        conv4_1 = Dropout(0.1)(conv4_1)

        #conv4_2
        conv4_2 = Conv2D(128, (5, 5), padding='same', name='conv4_2')(cnn3)
        conv4_2 = BatchNormalization()(conv4_2)
        conv4_2 = PReLU()(conv4_2)
        conv4_2 = MaxPooling2D(pool_size=max_pool_size)(conv4_2)
        conv4_2 = Dropout(0.1)(conv4_2)

        #conv4_3
        conv4_3 = Conv2D(128, (7, 7), padding='same', dilation_rate=(2, 2), name='conv4_3')(cnn3)
        conv4_3 = BatchNormalization()(conv4_3)
        conv4_3 = PReLU()(conv4_3)
        conv4_3 = MaxPooling2D(pool_size=max_pool_size)(conv4_3)
        conv4_3 = Dropout(0.1)(conv4_3)

        #conv4_4
        conv4_4 = Conv2D(128, (1, 1), padding='same', name='conv4_4')(cnn3)
        conv4_4 = BatchNormalization()(conv4_4)
        conv4_4 = PReLU()(conv4_4)
        conv4_4 = MaxPooling2D(pool_size=max_pool_size)(conv4_4)
        conv4_4 = Dropout(0.1)(conv4_4)


        cnn4 = concatenate([conv4_1, conv4_2, conv4_3, conv4_4])

        #conv5
        #conv5_1
        conv5_1 = Conv2D(128, (3, 3), padding='same', name='conv5_1')(cnn4)
        conv5_1 = BatchNormalization()(conv5_1)
        conv5_1 = PReLU()(conv5_1)
        conv5_1 = Dropout(0.1)(conv5_1)

        #conv5_2
        conv5_2 = Conv2D(128, (5, 5), padding='same', name='conv5_2')(cnn4)
        conv5_2 = BatchNormalization()(conv5_2)
        conv5_2 = PReLU()(conv5_2)
        conv5_2 = Dropout(0.1)(conv5_2)

        #conv5_3
        conv5_3 = Conv2D(128, (7, 7), padding='same', dilation_rate=(2, 2), name='conv5_3')(cnn4)
        conv5_3 = BatchNormalization()(conv5_3)
        conv5_3 = PReLU()(conv5_3)
        conv5_3 = Dropout(0.1)(conv5_3)

        #conv5_4
        conv5_4 = Conv2D(128, (1, 1), padding='same', name='conv5_4')(cnn4)
        conv5_4 = BatchNormalization()(conv5_4)
        conv5_4 = PReLU()(conv5_4)
        conv5_4 = Dropout(0.1)(conv5_4)

        x = concatenate([conv5_1, conv5_2, conv5_3, conv5_4])

        if include_top:
            x = GlobalAveragePooling2D(name='avg_pool')(x)
            x = Dense(classes, activation='softmax', name='predictions')(x)
        else:
            if pooling == 'avg':
                x = GlobalAveragePooling2D()(x)
            elif pooling == 'max':
                x = GlobalMaxPooling2D()(x)

        model = Model(img_input, x, name='similar_googlenet')

        return model

    def save_plot_model(self, path, name=''):
        if len(name) == 0:
            name = self.cnn_model + '_model.png'
        self.train_model.summary()
        plot_model(self.train_model, to_file=os.path.join(path, name), show_shapes=True)

    def fit_generator(self, train_generator, validate_generator, steps_per_epoch=100, epochs=1000, validation_steps=100,
                      log_dir='log_training.csv', valid_list='', batch_size=32, multithread=True):
        csv_logger = CSVLogger(log_dir)
        training_callback = TrainingCallback(predict_model=self.train_model, prefix=self.prefix_model_name,
                                             output_dir_path=self.output_model_path, input_shape=self.input_shape,
                                             valid_list=valid_list, batch_size=batch_size, classes=self.classes)

        learning_rates = np.linspace(0.005, 0.001, epochs) if self.load_weight_flag else np.linspace(0.02, 0.001, epochs)
        lr_cb = LearningRateScheduler(lambda epoch: float(learning_rates[epoch]))

        self.train_model.compile(loss="categorical_crossentropy",
                                 optimizer=SGD(lr=0.01, momentum=0.9, nesterov=True, clipnorm=5), metrics=["accuracy"])

        multiprocess_param = {True: {'um': True, 'ws': 6}, False: {'um': False, 'ws': 1}}
        self.train_model.fit_generator(generator=train_generator, epochs=epochs, steps_per_epoch=steps_per_epoch,
                                       validation_data=validate_generator, validation_steps=validation_steps,
                                       use_multiprocessing=multiprocess_param[multithread]['um'],
                                       workers=multiprocess_param[multithread]['ws'],
                                       callbacks=[csv_logger, lr_cb, training_callback])


if __name__ == '__main__':
    def check_model():
        # 'xception' 'densenet121' 'mobilenet' 'VGG16' 'similar_googlenet' 'xception_slim'
        model = CRNN(prefix_model_name='test', input_shape=(64, 256, 3), classes=['nocheck', 'check'],
                     output_model_path='./', gpu_count=1, load_model_weights_path='', cnn_model='mobilenet')
        model.save_plot_model('./')
    check_model()
