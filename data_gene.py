from six.moves import range
import numpy as np
import random
import cv2
import glob
import mojimoji
from PIL import ImageFont, ImageDraw, Image
import os
import string
import shutil
from text_gene import Text_gen

from utils import truncated_norm,  img_glob, resize_image_keeping_aspect_ratio, merge_images, to_rgb, normalize_text
from image_processing import saltpepper, random_gaussianBlur, random_transform_aspect, image_compression,\
    random_rotate, gamma_correction, emphasize_white, random_blur, random_line, extract_line, drawpoly, drawcircle  #cut_character
import keras
from keras.utils import Sequence


font_colors = ['#000000', '#333333', '#993300', '#663300', '#996600',
               '#333300', '#666633', '#666600', '#336633', '#006600', '#003300', '#336600',
               '#006633', '#003333', '#336666', '#006666', '#006699', '#003366', '#336699',
               '#0066cc', '#0066ff', '#003399', '#0033cc', '#0033ff',
               '#000033', '#333366', '#000066', '#333399', '#000099', '#6666cc',
               '#3333cc', '#0000cc', '#3333ff', '#0000ff', '#3300ff', '#3300cc',
               '#330099', '#6633cc', '#6600ff', '#330066', '#663399', '#6600cc',
               '#660099', '#9900cc', '#330033', '#663366', '#660066',
               '#990099', '#990066', '#660033', '#990033', '#330000', '#663333',
               '#660000', '#993333', '#990000', '#cc0000', '#ff0000']

bg_colors = ['#cccccc', '#ffffff', '#ff6633', '#ff9966', '#cc9966', '#ffcc99', '#ff9933', '#ff9900',
             '#ffcc66', '#ffcc33', '#ffcc00', '#cccc99', '#cccc66', '#cccc33', '#cccc00', '#ffffcc',
             '#ffff99', '#ffff66', '#ffff33', '#ffff00', '#ccff00', '#ccff33', '#99cc33', '#ccff66',
             '#99ff00', '#99cc66', '#ccff99', '#99ff33', '#66ff00', '#66cc33', '#99ff66', '#66ff33',
             '#669966', '#99cc99', '#66cc66', '#ccffcc', '#99ff99',
             '#66ff66', '#33ff33', '#33ff66', '#66ff99', '#00ff66', '#99ffcc',
             '#33ff99', '#66ffcc', '#33ffcc', '#00ffcc', '#66cccc',
             '#ccffff', '#99ffff', '#66ffff', '#33ffff', '#00ffff', '#00ccff', '#0099cc', '#33ccff',
             '#66ccff', '#99ccff', '#6699ff', '#ccccff', '#cc99ff', '#cc99cc', '#cc00cc', '#ffccff',
             '#ff99ff', '#ff66ff', '#ff66cc', '#cc6699', '#ff99cc', '#cc9999', '#ffcccc', '#ff9999',
             '#ff6666']

dot_circle_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255),
                     (128, 128, 128), (100, 100, 100), (150, 150, 150), (170, 170, 170), (200, 200, 200),
                     (220, 220, 220), (0, 0, 0)]

circle_colors = [(200, 200, 200), (210, 210, 210), (230, 230, 230), (240, 240, 240)]

class Data_gen(Sequence):
    def __init__(self, input_shape=(64, 256, 3), max_length=5, min_length=1, batch_size=32,
                 is_debug=False, dataset_path='../dataset/', is_marksheet=True):
        self.input_shape = input_shape
        self.is_debug = is_debug
        self.is_marksheet = is_marksheet
        self.parameter_list = self.__get_parameter()

        self.background_image_path = dataset_path + 'checkbox/background/'
        self.font_path = dataset_path + 'checkbox/font_data/'
        self.maru_image_path_list = img_glob(dataset_path + 'checkbox/template/maru/')
        self.check_image_path_list = img_glob(dataset_path + 'checkbox/template/check/')
        self.mark_image_path_list = img_glob(dataset_path + 'checkbox/template/mark/')
        self.asta_image_path_list = img_glob(dataset_path + 'checkbox/template/asta/')
        self.batch_size = batch_size
        self.classes = ['nocheck', 'check']
        self.background_images = self._get_background_images(self.background_image_path)
        self.check_background_path_list = img_glob(dataset_path + 'checkbox/background_check/')

        self.train_img_path_list1 = [img_glob(dataset_path + 'checkbox/checkbox_train/0/'),
                                     img_glob(dataset_path + 'checkbox/checkbox_train/1/')]

        self.train_img_path_list2 = [img_glob(dataset_path + 'checkbox/checkbox_datasets/train/0/'),
                                     img_glob(dataset_path + 'checkbox/checkbox_datasets/train/1/')]

        text_gen = Text_gen(max_length, min_length, dataset_path)
        self.text_maker = text_gen.gen_text
        self.fonts = glob.glob(self.font_path + '*.ttfx') + glob.glob(self.font_path + '*.ttc') + glob.glob(
            self.font_path + '*.ttf') + glob.glob(self.font_path + '*.otf')

        self.fail_image_path_list = img_glob(dataset_path + 'checkbox/failed_image')



    def __get_parameter(self):
        parameter_list = {'noise': {'no_noise': {'prob': 0.1},
                                    'random_rotate': {'prob': 1, 'sigma': 1},
                                    'random_transform_aspect': {'prob': 0.5},
                                    'random_line': {'prob': 0.1, 'dot_prob': 0.85, 'line_num': 1},
                                    'saltpepper': {'prob': 0.4, 'pepper': 0.0005},
                                    'binary': {'prob': 0.01, 'prob_blur': 0.5},
                                    'gaussianBlur': {'prob': 0.1, 'ave': 1, 'sigma': 0.5},
                                    'resise': {'prob': 0.2, 'ave': 0.9, 'sigma': 0.1},
                                    'compress': {'prob': 1, 'ave': 0.8, 'sigma': 0.2, 'is_near': 0.7},
                                    'gray': {'prob': 0.7},
                                    'crop': {'prob': 1, 'rate': 0.05},
                                    'background': {'prob': 0.5},
                                    'reverse': {'prob': 0.}
                                    },
                          }
        return parameter_list

    def get_classes(self):
        return self.classes

    def __getitem__(self, idx):
        batch_shape = (self.batch_size,) + self.input_shape

        inputs = np.zeros(batch_shape)
        labels = []
        for i in range(self.batch_size):
            r = random.uniform(0, 1)
            if self.is_marksheet:
                if r < 0.3:
                    img, label = self.box_check_maker()
                elif r < 0.55:
                    img, label = self.word_check_maker()
                elif r < 0.8:
                    img, label = self.marksheet_maker()
                else:
                    img, label = self.real_data_loader()
            else:
                if r < 0.0:
                    img, label = self.box_check_maker()
                elif r < 10.8:
                    img, label = self.word_check_maker()
                else:
                    img, label = self.real_data_loader()
            inputs[i] = img
            labels.append(0 if label == 0 else 1)
        inputs = inputs / 255.
        labels = keras.utils.to_categorical(labels, len(self.classes))
        return (inputs, labels)

    def __len__(self):
        return 10000

    def on_epoch_end(self):
        pass

    def marksheet_maker(self):
        label = random.choice([0, 3])
        img, mask_img = self.gen_mark_sheet_circle()
        mask_img = None if random.uniform(0, 1) < 0.1 else mask_img
        img = self.add_check(img, label, mask_img=mask_img)
        if random.uniform(0, 1) < 0.9:
            img = resize_image_keeping_aspect_ratio(img, self.input_shape)
        else:
            img = self.random_pad_img(img, self.input_shape)
        return img, label

    def box_check_maker(self):
        if random.uniform(0, 1) < 0.5:
            label = 0
        else:
            if random.uniform(0, 1) < 0.92:
                label = 2
            else:
                label = 1

        img = self.gen_box()
        img = self.add_check(img, label=label)
        img = self.add_noise(img)

        if random.uniform(0, 1) < 0.9:
            img = resize_image_keeping_aspect_ratio(img, self.input_shape)
        else:
            img = self.random_pad_img(img, self.input_shape)
        return img, label

    def word_check_maker(self):
        if random.uniform(0, 1) < 0.5:
            label = 0
        else:
            if random.uniform(0, 1) < 0.92:
                label = 1
            else:
                label = 2

        item = self.text_maker()
        prefix = random.choice(['・', ':', '[', '「', '(', '&', '_', '¥']) if random.uniform(0, 1) < 0.05 else ''
        if random.uniform(0, 1) < 0.005:
            number = prefix + str(truncated_norm(0, 99, 5, 15, to_int=True))
            string = number + random.choice([' ', '. ']) + item
            img = self.gen_rendering(string, circle=False)
            # print('gen_rendering true', img.shape)
            flag_rect = random.uniform(0, 1) < 0.8
        else:
            string = prefix + item
            img = self.gen_rendering(string, circle=random.uniform(0, 1) < 0.4)
            # print('gen_rendering false', img.shape)
            flag_rect = random.uniform(0, 1) < 0.1
        img = self.add_check(img, label=label, flag_rect=flag_rect)
        # print('add_check', img.shape)
        img = self.add_noise(img)
        # print('add_noise', img.shape)
        if random.uniform(0, 1) < 0.8:
            img = resize_image_keeping_aspect_ratio(img, self.input_shape)
        else:
            img = self.random_pad_img(img, self.input_shape)
        # print('word_check_maker', img.shape, label)
        return img, label

    def real_data_loader(self):
        label = 0 if random.uniform(0, 1) < 0.5 else 1
        while True:
            data_flag = random.uniform(0, 1) < 0.5
            img_list = self.train_img_path_list1[label] if data_flag else self.train_img_path_list2[label]
            img = cv2.imread(random.choice(img_list))
            if img is None or img.shape[0] <= 2 or img.shape[1] <= 2:
                continue
            if img.shape[0] > self.input_shape[0] or img.shape[1] > self.input_shape[1]:
                img = self.reshape_target(img, self.input_shape)

            if not data_flag:
                img = 255 - img
            break

        img = self.add_noise(img)
        if random.uniform(0, 1) < 0.5:
            img = self.random_pad_img(img, self.input_shape)
        else:
            img = resize_image_keeping_aspect_ratio(img, self.input_shape, remove_top_bottom_as_well=True)

        return img, label

    def random_pad_img(self, img, target_shape):
        rate_h = target_shape[0] / img.shape[0]
        rate_w = target_shape[1] / img.shape[1]

        rate = min(rate_h, rate_w)
        if rate < 1:
            shape = (int(max(1, rate * img.shape[1])), int(max(1, rate * img.shape[0])))
            img = cv2.resize(img, shape)

        out_img = np.zeros(target_shape, dtype=np.uint8) + 255
        h, w, _ = img.shape
        x = random.randint(0, self.input_shape[1] - w)
        y = random.randint(0, self.input_shape[0] - h)
        out_img[y:y+h, x:x+w] = img
        if random.uniform(0, 1) < self.parameter_list['noise']['background']['prob']:
            out_img = self.random_background(out_img)
        return out_img

    def random_background(self, image):
        back_image = random.choice(self.background_images)
        top = random.randint(0, back_image.shape[0] - image.shape[0])
        left = random.randint(0, back_image.shape[1] - image.shape[1])
        return merge_images(back_image[top:-1, left:-1], image, 0, 0)

    def _get_background_images(self, background_image_path):
        background_image_list = img_glob(background_image_path)
        images = []
        for background_path in background_image_list:
            img = cv2.imread(background_path)
            h, w, _ = img.shape
            rate_h = self.input_shape[0] / h
            rate_w = self.input_shape[1] / w
            rate = max(rate_h, rate_w)
            if rate > 1:
                shape = (int(max(1, rate * img.shape[1])), int(max(1, rate * img.shape[0])))
                img = cv2.resize(img, shape)
            images.append(img)
        return images

    def extract_boundbox(self, img):
        hist_h = [sum(i) for i in img]
        hist_w = [sum(i) for i in img.transpose()]
        hist_h_min = min(hist_h)
        hist_w_min = min(hist_w)
        x, y, width, height = 0, 0, 0, 0
        for (i, h) in enumerate(hist_h):
            if not h == hist_h_min:
                y = i
                break
        for (i, h) in enumerate(hist_w):
            if not h == hist_w_min:
                x = i
                break
        for (i, h) in enumerate(reversed(hist_h)):
            if not h == hist_h_min:
                height = len(hist_h) - i - y
                break
        for (i, h) in enumerate(reversed(hist_w)):
            if not h == hist_w_min:
                width = len(hist_w) - i - x
                break
        return x, y, width, height

    def gen_rendering(self, text, circle=False):
        np_image = self.text_rendering(text, circle)
        if circle:
            center = (int(np_image.shape[1] / 2), int(np_image.shape[0] / 2))
            if random.uniform(0, 1) < 0.5:
                color = (128, 128, 128) if random.uniform(0, 1) < 0.5 else random.choice(dot_circle_colors)
                np_image = drawcircle(np_image, center, center[0], center[1], color=color)
                # print('gen_rendering true', np_image.shape)
            else:
                val = random.randint(200, 254)
                color = (val, val, val)
                thickness = truncated_norm(1, 5, 2, 1, to_int=True)
                cv2.ellipse(np_image, center, (center[0], center[1]), 0, 0, 360, color, thickness)
                # print('gen_rendering false', np_image.shape)
        # print('gen_rendering', circle)
        return np_image

    def gen_box(self):
        max_size = 64
        if random.uniform(0, 1) < 0.3:
            img = cv2.imread(random.choice(self.check_background_path_list))
            img = cv2.resize(img, (max_size, max_size))
            return img

        img = np.zeros((max_size, max_size, 3), dtype=np.uint8) + 255
        if random.uniform(0, 1) < 0.5:
            return img
        size_h = truncated_norm(0, max_size, 50, 20, to_int=True)
        size_w = truncated_norm(0, max_size, 50, 20, to_int=True)
        thickness = truncated_norm(1, 4, 1, 2, to_int=True)

        tr = (int((max_size - size_h) / 2), int((max_size - size_w) / 2))
        bl = (tr[0] + size_h, tr[1] + size_w)
        img = cv2.rectangle(img, tr, bl, color=(0, 0, 0), thickness=thickness)
        return img

    def gen_mark_sheet_circle(self):
        max_size = 64
        img = np.zeros((max_size, max_size, 3), dtype=np.uint8) + 255
        mask_img = np.zeros((max_size, max_size), dtype=np.uint8)
        is_text = random.uniform(0, 1) < 0.9
        if is_text:
            if random.uniform(0, 1) < 0.8:
                text = random.choice('123456789')
            else:
                text = random.choice(string.ascii_letters)

            txt_img = self.text_rendering(text, back_white=True)

        r = random.uniform(0, 1)
        if r < 0.2:
            circle_size = (truncated_norm(10, 32, 25, 8, to_int=True), truncated_norm(10, 32, 25, 8, to_int=True))
        elif r < 0.6:
            circle_size = (truncated_norm(10, 32, 10, 4, to_int=True), truncated_norm(10, 32, 25, 4, to_int=True))
        else:
            circle_size = (truncated_norm(10, 32, 25, 4, to_int=True), truncated_norm(10, 32, 10, 4, to_int=True))
        circle_center = (int(64 / 2), int(64 / 2))
        circle_color = to_rgb(random.choice(font_colors))
        circle_thickness = truncated_norm(1, 5, 1, 1, to_int=True)

        if is_text:
            size = truncated_norm(15, 64, min(circle_size), 7, to_int=True)
            rate = size / max(txt_img.shape[:2])
            shape = (int(max(1, rate * txt_img.shape[1])), int(max(1, rate * txt_img.shape[0])))
            txt_img = cv2.resize(txt_img, shape)
            h, w, _ = txt_img.shape
            top = int((max_size - h) / 2)
            left = int((max_size - w) / 2)
            img[top:top + h, left:left + w] = txt_img

        if random.uniform(0, 1) < 0.9:
            if random.uniform(0, 1) < 0.5:
                cv2.ellipse(img, circle_center, circle_size, 0, 0, 360, circle_color, circle_thickness)
            else:
                img = drawcircle(img, circle_center, circle_size[0], circle_size[1], color=circle_color)

            cv2.ellipse(mask_img, circle_center, circle_size, 0, 0, 360, 255, -1)
        else:
            lt = (int(circle_center[0]-(circle_size[0]/2)), int(circle_center[1]-(circle_size[1]/2)))
            rb = (int(circle_center[0]+(circle_size[0]/2)), int(circle_center[1]+(circle_size[1]/2)))
            cv2.rectangle(img, lt, rb, circle_color, circle_thickness)
            cv2.rectangle(mask_img, lt, rb, 255, -1)

        return img, mask_img

    def text_rendering(self, text, circle=False, back_white=False, vert=False):
        def horizontal(text, text_len, font_path, circle, back_white):
            window_height = 64
            window_width = int(font_size * 1.2 * text_len)

            if circle:
                color = '#000000'
                bg_color = '#ffffff'
                margin = 20
            else:
                color = '#000000' if random.uniform(0, 1) < 0.5 else random.choice(font_colors)
                bg_color = '#ffffff' if random.uniform(0, 1) < 0.5 else random.choice(bg_colors)
                margin = 16

            if back_white:
                bg_color = '#ffffff'

            img = Image.new('RGB', (window_width, window_height), to_rgb(bg_color))
            draw = ImageDraw.Draw(img)
            font = ImageFont.truetype(font_path, font_size)
            draw.text((5, 5), text, fill=color, font=font)
            np_image = np.array(img)
            gray = cv2.cvtColor(np_image, cv2.COLOR_BGR2GRAY)
            x, y, w, h = self.extract_boundbox(255 - gray)

            x = max(0, x - int(margin / 2))
            w += margin
            y = max(0, y - int(margin / 2))
            h += margin

            np_image = np_image[y:y + h, x:x + w]
            return np_image

        def vertical(text, text_len, font_path, circle, back_white):
            imgs = []
            for char in text:
                char_img = self.generate_char_img(fontname=font_path, char=char)
                char_img = 255 - char_img
                char_img = cv2.cvtColor(char_img, cv2.COLOR_GRAY2BGR)
                imgs.append(char_img)
            return cv2.vconcat(imgs)

        font_size = 50

        text_len = len(text)
        if len(text) < 5 and random.uniform(0, 1) < 0.1:
            space = random.choice([' ', '  ', '  '])
            text = ''.join([t + space for t in text])
            text_len += int((len(text)-1)*len(space)*0.5)

        font_path = random.choice(self.fonts)

        if random.uniform(0, 1) < 0.1:
            text = mojimoji.zen_to_han(text)

        if text_len <= 3 and random.uniform(0, 1) < 0.2:
            img = vertical(text, text_len, font_path, circle, back_white)
            # print('vertical', img.shape, text)
        else:
            img = horizontal(text, text_len, font_path, circle, back_white)
            # print('horizontal', img.shape, text)

        return img

    def generate_char_img(self, char=None, fontname=None, size=(64, 64), fontsize=0.9, is_color=False):
        def __pil2cv(pil_image):
            cv_image = np.asarray(pil_image)[:, :, ::-1].copy()
            return cv_image

        def __cv2pil(cv_image):
            cv_image = cv_image[:, :, ::-1].copy()
            pil_image = Image.fromarray(cv_image)
            return pil_image
        if fontname is None:
            target_fontname = random.choice(self.fonts)
        else:
            target_fontname = fontname

        if is_color:
            (r, g, b) = random.randint(0, 255), random.randint(0,255), random.randint(0,255)
        else:
            (r, g, b) = 0, 0, 0
        background_image = np.zeros([size[0], size[1], 3], dtype=np.uint8)
        background_image[:, :, (1, 2)] = r
        background_image[:, :, (0, 2)] = g
        background_image[:, :, (1, 1)] = b
        background_image = __cv2pil(background_image)
        draw = ImageDraw.Draw(background_image)
        fontsize = int(size[0]*fontsize)
        font = ImageFont.truetype(target_fontname, fontsize)

        # adjust charactor position.
        char_displaysize = font.getsize(char)
        offset = tuple((si-sc)//2 for si, sc in zip(size, char_displaysize))

        # adjust offset, half value is right size for height axis.
        if is_color:
            color = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
        else:
            color = 255, 255, 255
        draw.text((offset[0], offset[1]//2), char, font=font, fill=color)

        # verify image
        cv_image = __pil2cv(background_image)
        if not is_color:
            cv_image = cv2.cvtColor(cv_image, cv2.COLOR_RGB2GRAY)

        result = 1000
        for fail_img_path in self.fail_image_path_list:
            result_temp = (cv_image - cv2.imread(fail_img_path, cv2.IMREAD_GRAYSCALE)).sum()
            result = min(result, result_temp)

        # verify
        if result == 0:
            cv_image = self.generate_char_img(char)
        if not (char == '\u0020' or char == '\u3000' or char =='\u005f') and cv_image.max() == 0:
            cv_image = self.generate_char_img(char)
        return cv_image

    def add_check(self, src_img, label, flag_rect=True, mask_img=None):
        if not label == 0:
            flag_binary = random.uniform(0, 1) < 0.4
            if label == 1:
                img = cv2.imread(random.choice(self.maru_image_path_list))
            elif label == 2:
                if random.uniform(0, 1) < 0.98:
                    img = cv2.imread(random.choice(self.check_image_path_list))
                else:
                    img = cv2.imread(random.choice(self.asta_image_path_list))
                    img = 255 - img
            elif label == 3:
                img = cv2.imread(random.choice(self.mark_image_path_list))

            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
            x, y, w, h = cv2.boundingRect(255 - gray)
            if flag_binary:
                img = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

            img = img[y:y + h, x:x + w]

            if mask_img is None:
                if flag_rect:
                    length = min([src_img.shape[0], src_img.shape[1]]) - 1
                    length = truncated_norm(5, length, 3*length/4, 8, to_int=True)
                    img = cv2.resize(img, (length, length))
                    pad_img = np.zeros(src_img.shape, dtype=np.uint8) + 255

                    top = truncated_norm(0, src_img.shape[0] - length, 0, 20, to_int=True)
                    left = truncated_norm(0, src_img.shape[1] - length, 0, 20, to_int=True)

                    pad_img[top: top+img.shape[0], left: left+img.shape[1]] = img
                    src_img = merge_images(src_img, pad_img, 0, 0)
                elif random.uniform(0, 1) < 0.7:
                    pad_img = np.zeros(src_img.shape, dtype=np.uint8) + 255
                    size = (truncated_norm(5, src_img.shape[1]-1, 3*src_img.shape[1]/4, src_img.shape[1]/4, to_int=True),
                            truncated_norm(5, src_img.shape[0]-1, 3*src_img.shape[0]/4, src_img.shape[0]/4, to_int=True))
                    img = cv2.resize(img, size)

                    top = truncated_norm(0, src_img.shape[0] - size[1], 0, 20, to_int=True)
                    left = truncated_norm(0, src_img.shape[1] - size[0], 0, 20, to_int=True)

                    pad_img[top: top+img.shape[0], left: left+img.shape[1]] = img

                    src_img = merge_images(src_img, img, 0, 0)
                else:
                    img = cv2.resize(img, (src_img.shape[1], src_img.shape[0]))
                    src_img = merge_images(src_img, img, 0, 0)
            else:
                img = cv2.resize(img, (src_img.shape[1], src_img.shape[0]))
                img = cv2.bitwise_and(img, img, mask=mask_img)
                img[mask_img == 0] = 255
                src_img = merge_images(src_img, img, 0, 0)
        return src_img

    def reshape_target(self, img, target):
        rate_h = target[0] / img.shape[0]
        rate_w = target[1] / img.shape[1]
        rate = min(rate_h, rate_w)
        shape = (int(max(1, rate*img.shape[1])), int(max(1, rate*img.shape[0])))
        img = cv2.resize(img, shape)
        return img

    def add_noise(self, img, blank=False):
        img = self.reshape_target(img, self.input_shape)

        if random.uniform(0, 1) < self.parameter_list['noise']['gray']['prob']:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

        if random.uniform(0, 1) < self.parameter_list['noise']['no_noise']['prob']:
            return img

        if random.uniform(0, 1) < self.parameter_list['noise']['random_rotate']['prob']:
            img = random_rotate(img, rad_sigma=self.parameter_list['noise']['random_rotate']['sigma'])

        if random.uniform(0, 1) < self.parameter_list['noise']['random_transform_aspect']['prob']:
            img = random_transform_aspect(img)

        if random.uniform(0, 1) < self.parameter_list['noise']['random_line']['prob']:
            if random.uniform(0, 1) < self.parameter_list['noise']['random_line']['dot_prob']:
                img = random_line(img,
                                  line_num=truncated_norm(1, 5, self.parameter_list['noise']['random_line']['line_num'],
                                                          1, to_int=True), thick_ave=1, style='dot')
            else:
                img = random_line(img,
                                  line_num=truncated_norm(1, 5, self.parameter_list['noise']['random_line']['line_num'],
                                                          1, to_int=True), thick_ave=1)

        if random.uniform(0, 1) < self.parameter_list['noise']['saltpepper']['prob']:
            img = saltpepper(img, pepper=self.parameter_list['noise']['saltpepper']['pepper'])

        h, w, _ = img.shape
        if h > (self.input_shape[0] * 0.9):
            rate = truncated_norm(0.3, 1, self.parameter_list['noise']['compress']['ave'],
                                  self.parameter_list['noise']['compress']['sigma'])
        else:
            rate = truncated_norm(0.8, 1, 1, 0.05)

        shape = (int(max(1, rate*img.shape[1])), int(max(1, rate*img.shape[0])))
        img = cv2.resize(img, shape)

        if not blank and random.uniform(0, 1) < self.parameter_list['noise']['binary']['prob']:
            if random.uniform(0, 1) < self.parameter_list['noise']['binary']['prob_blur']:
                img = random_blur(img)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

        if random.uniform(0, 1) < self.parameter_list['noise']['gaussianBlur']['prob']:
            img = random_gaussianBlur(img, ave=self.parameter_list['noise']['gaussianBlur']['ave'],
                                      sig=self.parameter_list['noise']['gaussianBlur']['sigma'])

        if random.uniform(0, 1) < self.parameter_list['noise']['compress']['is_near']:
            img = cv2.resize(img, (w, h), interpolation=cv2.INTER_NEAREST)
        else:
            img = cv2.resize(img, (w, h))

        if random.uniform(0, 1) < self.parameter_list['noise']['resise']['prob']:
            rate = truncated_norm(0.3, 1.0, self.parameter_list['noise']['resise']['ave'],
                                  self.parameter_list['noise']['resise']['sigma'])
            shape = (int(max(1, rate * img.shape[1])), int(max(1, rate * img.shape[0])))
            img = cv2.resize(img, shape)

        if random.uniform(0, 1) < self.parameter_list['noise']['reverse']['prob']:
            img = 255 - img

        if random.uniform(0, 1) < self.parameter_list['noise']['crop']['prob']:
            t = random.randint(0, int(img.shape[0]*self.parameter_list['noise']['crop']['rate']))
            b = random.randint(int(img.shape[0]*(1-self.parameter_list['noise']['crop']['rate'])), img.shape[0])
            l = random.randint(0, int(img.shape[1]*self.parameter_list['noise']['crop']['rate']))
            r = random.randint(int(img.shape[1]*(1-self.parameter_list['noise']['crop']['rate'])), img.shape[1])
            img = img[t:b, l:r]

        return img

    def gen_valid(self, valid_path):
        batch_shape = (self.batch_size,) + self.input_shape

        while True:
            inputs = np.zeros(batch_shape)
            labels = []

            # label_dict = {'0': 'nocheck', '1': 'check'}
            for i in range(self.batch_size):
                label = random.choice([0, 1])
                img_path = random.choice(img_glob(valid_path+str(label)+'/'))
                img = cv2.imread(img_path)
                img = resize_image_keeping_aspect_ratio(img, self.input_shape, remove_top_bottom_as_well=True)

                inputs[i] = img
                labels.append(0 if label == 0 else 1)
            labels = keras.utils.to_categorical(labels, len(self.classes))
            yield (inputs, labels)

    def check_batch(self, inputs, labels, path='batch/', label=True):
        if os.path.exists(path):
            shutil.rmtree(path)
        os.makedirs(path, exist_ok=True)
        for (i, img) in enumerate(inputs):
            if label:
                label_str = self.classes[np.argmax(labels[i])]
                file_name = path + str(i).zfill(2) + '_' + label_str + '.png'
            else:
                file_name = path + str(i).zfill(2) + '.png'
            cv2.imwrite(file_name, (img) * 255)

    def test_generator(self, count=100):
        for i in range(count):
            print(i)
            self.__getitem__(i)


if __name__ == '__main__':
    dataset_path = '/Volumes/TOSHIBACANVIOCONNECT/dataset/'
    valid_path = dataset_path + 'checkbox/circle_valid/'
    is_marksheet = False
    batch_size = 100
    data_gen = Data_gen(dataset_path=dataset_path, batch_size=batch_size, is_marksheet=is_marksheet)

    for _ in range(10):
        inputs, labels = data_gen.__getitem__(1)
    # inputs, labels = next(data_gen.gen_valid(valid_path=valid_path))
    # print(type(labels))
    data_gen.check_batch(inputs, labels)
    # data_gen.test_generator()
