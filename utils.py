# coding=utf-8
from __future__ import division
import glob
import os
import re
import cv2
import numpy as np
import unicodedata
import Levenshtein
import json
import uuid
import random
import math
import shutil
import redis
import scipy.stats as stats
from keras.models import Model
from keras.layers import concatenate, Lambda
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter
import tensorflow as tf
from collections import Counter
import tensorflow as tf
from tensorflow.python.ops import ctc_ops as ctc
from keras import backend as K
import json

BACKGROUND_PATH = os.getenv('BACKGROUND_PATH', 'background')

def to_rgb(c):
    return int(c[1:3], 16), int(c[3:5], 16), int(c[5:7], 16)


def norm_batch(images, constant_input_shape=(64, 1024, 3)):
    imgs = np.array([]).reshape(0, constant_input_shape[0], constant_input_shape[1], constant_input_shape[2])
    for img in images:
        img = resize_image_keeping_aspect_ratio(img, target_shape=constant_input_shape, remove_top_bottom_as_well=True)
        img = img / 255.
        img = np.expand_dims(img, axis=0)
        imgs = np.append(imgs, img, axis=0)
    return imgs


def norm_batch_(images, constant_input_shape=(64, 1024, 3)):
    input_data = np.zeros((len(images),) + constant_input_shape, dtype=np.float64)
    for index, img in enumerate(images):
        img = resize_image_keeping_aspect_ratio(img, target_shape=constant_input_shape, remove_top_bottom_as_well=False)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        img = img / 255.0
        input_data[index, :, :, :] = img
    return input_data


def resize_image_keeping_aspect_ratio(src_image, target_shape, remove_top_bottom_as_well=False):
    if src_image.shape[0] * src_image.shape[1] <= 0:
        return src_image

    # first, remove the redundant space around the image
    if len(src_image.shape) == 2 or src_image.shape[2] == 1:
        thresh = cv2.threshold(src_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        x, y, w, h = cv2.boundingRect(255 - thresh)
        w = max(1, w)
        h = max(1, h)
        if remove_top_bottom_as_well:
            src_image = src_image[y: y + h, x: x + w]
        else:
            src_image = src_image[:, x: x + w]
    else:
        gray_image = cv2.cvtColor(src_image, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        x, y, w, h = cv2.boundingRect(255 - thresh)
        w = max(1, w)
        h = max(1, h)
        if remove_top_bottom_as_well:
            src_image = src_image[y: y + h, x: x + w, :]
        else:
            src_image = src_image[:, x: x + w, :]

    # change the height to the target one
    src_image = cv2.resize(src_image,
                           (max(1, src_image.shape[1] * target_shape[0] // src_image.shape[0]),
                            target_shape[0]), interpolation=cv2.INTER_CUBIC)

    # make the image shrink the width if it is still long
    if target_shape[1] < src_image.shape[1]:
        src_image = cv2.resize(src_image, (target_shape[1], target_shape[0]),
                               interpolation=cv2.INTER_CUBIC)

    # otherwise, add blank space to the right so that the size of the image can be the given one
    elif src_image.shape[1] < target_shape[1]:
        src_image = set_margin(src_image, margins=(0, target_shape[1] - src_image.shape[1], 0, 0))

    return src_image


def set_margin(src_image, margins=(10, 10, 10, 10), background_color=255, color=True):
    if color:
        canvas = np.zeros((src_image.shape[0] + margins[0] + margins[2],
                           src_image.shape[1] + margins[1] + margins[3], 3), np.uint8) + background_color
    else:
        canvas = np.zeros((src_image.shape[0] + margins[0] + margins[2],
                           src_image.shape[1] + margins[1] + margins[3]), np.uint8) + background_color
    y1, y2 = margins[0], margins[0] + src_image.shape[0]
    x1, x2 = margins[3], margins[3] + src_image.shape[1]
    canvas[y1: y2, x1: x2] = src_image
    return canvas


def img_glob(path):
    ext = '.*\.(jpg|png|bmp)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def json_glob(path):
    ext = '.*\.(json)'
    path = os.path.join(path, '*')
    files = glob.glob(path)
    files = [f for f in files if re.search(ext, f, re.IGNORECASE)]
    return files


def normalize_text(text, remove_blank=True):
    text = unicodedata.normalize('NFKC', text).replace('−', '-')
    if remove_blank:
        text = text.replace(' ', '')
    return text


def merge_images(source_image, target_image, top, left, operator=np.minimum):
    _top = min(top + source_image.shape[0], target_image.shape[0])
    _left = min(left + source_image.shape[1], target_image.shape[1])
    if top < _top and left < _left:
        target_image[top: _top, left: _left] = operator(target_image[top: _top, left: _left],
                                                        source_image[0: _top - top, 0: _left - left])

    return target_image


def truncated_norm(lower, upper, mu, sigma, to_int=False):
    value = stats.truncnorm((lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma).rvs()
    return int(round(value)) if to_int else value
