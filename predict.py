import cv2
import os
import numpy as np
import h5py
from keras.models import load_model
from keras.applications import mobilenet
import shutil
import json
from utils import img_glob, norm_batch, norm_batch_


def predict(model, batch_size, img_path, answer, input_shape, classes, reverse=False):
    img_path_list = img_glob(img_path)
    perfect_scores, results = [], []
    count = 1
    for i in range(0, len(img_path_list), batch_size):
        imgs = [cv2.imread(image_path) for image_path in img_path_list[i:i + batch_size]]
        imgs = norm_batch(imgs, constant_input_shape=input_shape)
        if reverse:
            imgs = 1 - imgs
        outs = model.predict(imgs)

        texts, likelihoods = [], []
        for o in outs:
            max_index = o.argmax()
            texts.append(classes[max_index])
            likelihoods.append(o[max_index])

        for (image_path, text, likelihood) in zip(img_path_list[i:i + batch_size], texts, likelihoods):
            prefix = '○' if answer == text else '×'
            print(count, '/', len(img_path_list), prefix, answer, text, likelihood, image_path.split('/')[-1])
            perfect_scores.append(1 if answer == text else 0)
            results.append([text, answer, image_path, likelihood])
            count += 1
    return perfect_scores, results


def predict_copy(model, batch_size, img_path, answer, input_shape, classes, reverse=False, copy=True):
    img_path_list = img_glob(img_path)
    img_path_list = sorted(img_path_list)
    perfect_scores, results = [], []
    count = 1
    miss_img_path = 'miss_images_/'
    os.makedirs(miss_img_path, exist_ok=True)

    for i in range(0, len(img_path_list), batch_size):
        imgs = [cv2.imread(image_path) for image_path in img_path_list[i:i + batch_size]]
        imgs = norm_batch(imgs, constant_input_shape=input_shape)
        if reverse:
            imgs = 1 - imgs
        outs = model.predict(imgs)

        texts, likelihoods = [], []
        for o in outs:
            max_index = o.argmax()
            texts.append(classes[max_index])
            likelihoods.append(o[max_index])

        for (image_path, text, likelihood) in zip(img_path_list[i:i + batch_size], texts, likelihoods):
            prefix = '○' if answer == text else '×'
            print_str = '{}/{}\t{}\t{}\t{}\t{}\t{}'.format(count, len(img_path_list), prefix, answer, text, likelihood,
                                                           image_path.split('/')[-1])
            print(print_str)
            perfect_scores.append(1 if answer == text else 0)
            results.append([text, answer, image_path, likelihood])
            if copy and not answer == text:
                out_filename = '{}_{}_{}_{:.3f}_{:03d}.png'.format(image_path.split('/')[-3], answer, text, likelihood, count)
                output_name = os.path.join(miss_img_path, out_filename)
                shutil.copy(image_path, output_name)

            count += 1
    return perfect_scores, results


def output_log(log_file_name, results):
    f = open(log_file_name, 'w')
    for (pred, ans, path, likelihood) in results:
        prefix = '○' if ans == pred else '×'
        f.write(prefix + '\t' + ans + '\t' + pred + '\t' + path + '\t' + str(likelihood) + '\n')
    f.close()


# model_path = 'model/05281622_checkbox_mobilenet_epoch_011_loss_0.01566_valid_acc_0.89684.hdf5'
model_path = 'model/06011047_checkbox_mobilenet_epoch_169_loss_0.00937_valid_acc_0.91832.hdf5'
model = load_model(model_path, custom_objects={'relu6': mobilenet.relu6})
input_shape = (64, 256, 3)
batch_size = 4
# valid_path = '../dataset/checkbox/checkbox_valid'
# valid_path = '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/checkbox_valid_old'
valid_path = '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/checkbox_valid1'
valid_path_list = [{'path': '/Users/masayaiwasaki/Downloads/check_temp/', 'answer': 'check'},
                   {'path': '/Users/masayaiwasaki/Downloads/marksheet_test/0/', 'answer': 'nocheck'},
                   {'path': '/Users/masayaiwasaki/Downloads/marksheet_test/1/', 'answer': 'check'},
                   ]
valid_path_list = ['/Users/masayaiwasaki/Downloads/marksheet_test',
                   '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/checkbox_valid_old',
                   '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/checkbox_valid1',
                   '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/checkbox_valid2',
                   '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/marksheet'
                   ]
valid_path_list = ['/Users/masayaiwasaki/Downloads/checkbox2',
                   '/Users/masayaiwasaki/Downloads/checkbox']

valid_path_list = ['/Users/masayaiwasaki/Downloads/chechbox_issue']

# valid_path_list = ['/Users/masayaiwasaki/Downloads/checkbox']

# valid_path = '/Volumes/TOSHIBACANVIOCONNECT/dataset/checkbox/checkbox_valid2'
log_name = model_path.split('/')[-1].replace('.hdf5', '.txt')

with h5py.File(model_path, 'r') as h5file:
    classes = [s.decode('utf8') for s in np.asarray(
        h5file[os.path.join('ai_inside', 'classes')].value).flatten()]

perfect_scores_0, results_0 = [], []
perfect_scores_1, results_1 = [], []
log_text = []
for valid_path in valid_path_list:
    print()
    if isinstance(valid_path, dict):
        if valid_path['answer'] == 'check':
            perfect_scores_, _ = predict_copy(model=model, batch_size=batch_size, img_path=valid_path['path'],
                                              answer=classes[1], input_shape=input_shape, classes=classes,
                                              reverse=False, copy=True)
            perfect_scores_1 += perfect_scores_
        else:
            perfect_scores_, _ = predict_copy(model=model, batch_size=batch_size, img_path=valid_path['path'],
                                              answer=classes[0], input_shape=input_shape, classes=classes,
                                              reverse=False, copy=True)
            perfect_scores_0 += perfect_scores_
    else:
        perfect_scores_, _ = predict_copy(model=model, batch_size=batch_size, img_path=os.path.join(valid_path, '1/'),
                                          answer=classes[1], input_shape=input_shape, classes=classes, reverse=False,
                                          copy=True)
        perfect_scores__, _ = predict_copy(model=model, batch_size=batch_size, img_path=os.path.join(valid_path, '0/'),
                                           answer=classes[0], input_shape=input_shape, classes=classes, reverse=False,
                                           copy=True)
        perfect_scores_1 += perfect_scores_
        perfect_scores_0 += perfect_scores__
        perfect_scores = perfect_scores_ + perfect_scores__
        perfect_score = 1.0 * sum(perfect_scores) / len(perfect_scores)

        print(valid_path.split('/')[-1], perfect_score)
        log_text.append(valid_path.split('/')[-1] + '\t' + str(len(perfect_scores)) + '\t' + str(perfect_score))

perfect_scores = perfect_scores_0 + perfect_scores_1
results = results_0 + results_1

perfect_score = 1.0 * sum(perfect_scores) / len(perfect_scores)

output_log(log_name, results)
tp = sum(perfect_scores_1)
fn = len(perfect_scores_1) - tp
tn = sum(perfect_scores_0)
fp = len(perfect_scores_0) - tn

for log in log_text:
    print(log)
print('file' + '\t' + str(len(perfect_scores)))
print('acc' + '\t' + str(perfect_score))
print('confusion matrix')
print(str(tp) + '\t' + str(fn))
print(str(fp) + '\t' + str(tn))
# '''
